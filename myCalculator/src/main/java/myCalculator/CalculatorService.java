package myCalculator;

public class CalculatorService {
	public static float calculate(Calculator calculator) {
        float result = 0;
        switch (calculator.getOperation()) {
            case DIV:
                result = calculator.getNum1() / calculator.getNum2();
                break;
            case MUL:
                result = calculator.getNum1() * calculator.getNum2();
                break;
            case SUB:
                result = calculator.getNum1() - calculator.getNum2();
                break;
            case SUM:
                result = calculator.getNum1() + calculator.getNum2();
                break;
        }
        return result;
    }
}
