package myCalculator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CalculatorController {
	
	@GetMapping("/calculator")
    public String indexPage(Model model) {
        model.addAttribute("calculator", new Calculator());
        return "calculator";
    }

    @PostMapping("/calculator")
    public String calculatorSubmit(@ModelAttribute Calculator calculator) {
        float result = CalculatorService.calculate(calculator);
        calculator.setResult(result);
        return "calculator";
    }

}
