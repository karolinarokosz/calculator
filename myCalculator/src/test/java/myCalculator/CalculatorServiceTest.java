package myCalculator;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CalculatorServiceTest {

    @Test
    public void testAdd(){
        Calculator calc = new Calculator();
        calc.setNum1(3);
        calc.setNum2(5);
        calc.setOperation(Operations.SUM);

        float result = CalculatorService.calculate(calc);

        assertThat(result).isEqualTo(8.0f);
    }

    @Test
    public void testMultiply(){
        Calculator calc = new Calculator();
        calc.setNum1(3);
        calc.setNum2(5);
        calc.setOperation(Operations.MUL);

        float result = CalculatorService.calculate(calc);

        assertThat(result).isEqualTo(15f);
    }
}